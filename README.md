# UeberauthOidccCertification

Phoenix implementation of
[ueberauth_oidcc](https://hexdocs.pm/ueberauth_oidcc/readme.html) to validate
that it passes the [OpenID Conformance Suite](https://www.certification.openid.net/).

## Getting started (Automated)

1. Register a [token](https://www.certification.openid.net/tokens.html)
2. Add the token to your local environment (such as with direnv) as `CONFORMANCE_TOKEN`
3. `mix test test/ueberauth_oidcc_certification_web/conformance/`

## Getting started (Manual)

1. Register a [test plan](https://www.certification.openid.net/schedule-test.html)
  - Test Plan: "OpenID Connect Core Client Tests"
  - Client Authentication Type: "client_secret_post"
  - Request Type: "plain_http_request"
  - Response Type: "code"
  - Response Mode: "default"
  - Client Registration Type: "static_client"
  - redirect_url: "http://localhost:4000/auth/openid/callback"
  - ... (other values can be whatever you choose)
2. Add the configuration to your local environment (such as with direnv):
  - `CLIENT_ID`: as configured in the test plan
  - `CLIENT_SECRET`: as configured in the test plan
  - `ISSUER`: given once you've registered the test plan
3. Run the server

``` sh
mix deps.get
mix assets.setup
mix assets.build
mix phx.server
```

4. Run through each step of the test plan, clicking "Sign in to the Conformance Suite"
  - `oidcc-client-test-discovery-openid-config`: click "Refresh Configuration" instead
  - `oidcc-client-test-discovery-jwks-uri-keys`: click "Refresh Configuration" instead
  - `oidcc-client-test-discovery-issuer-mismatch`: click "Refresh Configuration" instead
  - `oidcc-client-test-aggregated-claims`: Known failure, see [#2098](https://bitbucket.org/openid/connect/issues/2098/query-over)
  - Not implemented:
    - `oidcc-client-test-userinfo-bearer-body` (we only pass the token as a header)
    - `oidcc-client-test-discovery-webfinger-acct` (we don't support webfinger)
    - `oidcc-client-test-discovery-webfinger-url` (we don't support webfinger)

