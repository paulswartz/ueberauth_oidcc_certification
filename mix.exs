defmodule UeberauthOidccCertification.MixProject do
  use Mix.Project

  def project do
    [
      app: :ueberauth_oidcc_certification,
      version: "0.1.0",
      elixir: "~> 1.14",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {UeberauthOidccCertification.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.7.10"},
      {:phoenix_html, "~> 3.3"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:phoenix_live_view, "~> 0.20.1"},
      {:floki, ">= 0.30.0", only: :test},
      {:phoenix_live_dashboard, "~> 0.8.2"},
      {:esbuild, "~> 0.8", runtime: Mix.env() == :dev},
      {:tailwind, "~> 0.2.0", runtime: Mix.env() == :dev},
      {:credo, "~> 1.7", only: :dev},
      {:telemetry_metrics, "~> 0.6"},
      {:telemetry_poller, "~> 1.0"},
      {:jason, "~> 1.2"},
      {:bandit, ">= 0.0.0"},
      {:tesla, "~> 1.8"},
      {:req, "~> 0.4"},
      {:castore, "~> 1.0"},
      {:wallaby, "~> 0.30.6"},
      {:ueberauth, "~> 0.10"},
      ueberauth_oidcc_dep(),
      oidcc_dep(),
      {:ueberauth_google, "~> 0.12.1"}
      # {:ueberauth_google, path: "../../github/ueberauth_google"}
    ]
  end

  defp ueberauth_oidcc_dep do
    path = System.get_env("UEBERAUTH_OIDCC_PATH", "")
    ref = System.get_env("UEBERAUTH_OIDCC_REF", "")

    cond do
      path != "" ->
        {:ueberauth_oidcc, path: path, override: true}

      ref != "" and ref != "release" ->
        {:ueberauth_oidcc,
         git: "https://gitlab.com/paulswartz/ueberauth_oidcc.git", ref: ref, override: true}

      true ->
        {:ueberauth_oidcc, ">= 0.4.0", override: true}
    end
  end

  defp oidcc_dep do
    path = System.get_env("OIDCC_PATH", "")
    ref = System.get_env("OIDCC_REF", "")

    cond do
      path != "" ->
        {:oidcc, path: path, override: true}

      ref != "" and ref != "release" ->
        {:oidcc, github: "erlef/oidcc", ref: ref, override: true}

      true ->
        {:oidcc, ">= 3.2.0", override: true}
    end
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to install project dependencies and perform other setup tasks, run:
  #
  #     $ mix setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      setup: ["deps.get", "assets.setup", "assets.build"],
      "assets.setup": ["tailwind.install --if-missing", "esbuild.install --if-missing"],
      "assets.build": ["tailwind default", "esbuild default"],
      "assets.deploy": ["tailwind default --minify", "esbuild default --minify", "phx.digest"]
    ]
  end
end
