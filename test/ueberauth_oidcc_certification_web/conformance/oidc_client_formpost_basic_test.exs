defmodule OidcClientFormpostBasicTest do
  @moduledoc false

  use UeberauthOidccCertification.DynamicConformanceTest,
    moduletag: :release,
    describe: "oidcc-client-formpost-basic-certification-test-plan (plain_http_request)",
    test_plan: "oidcc-client-formpost-basic-certification-test-plan",
    ueberauth_config: [
      callback_methods: ["POST"],
      session_same_site: "None"
    ],
    config: %{
      "alias" => "ueberauth_oidcc_formpost_plain",
      "description" => "UeberauthOidcc (formpost certification, form_post, plain_http_request)"
    },
    variant: %{
      "client_registration" => "dynamic_client",
      "request_type" => "plain_http_request"
    },
    overrides: %{
      "oidcc-client-test-nonce-invalid" => :login_fail,
      "oidcc-client-test-missing-iat" => :login_fail,
      "oidcc-client-test-invalid-aud" => :login_fail,
      "oidcc-client-test-missing-sub" => :login_fail,
      "oidcc-client-test-userinfo-invalid-sub" => :login_fail,
      "oidcc-client-test-invalid-sig-rs256" => :login_fail,
      "oidcc-client-test-invalid-iss" => :login_fail
    }
end
