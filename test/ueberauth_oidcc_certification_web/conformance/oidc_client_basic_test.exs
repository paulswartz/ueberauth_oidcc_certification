defmodule OidcClientBasicTest do
  @moduledoc false
  use ExUnit.Case, async: true

  use UeberauthOidccCertification.DynamicConformanceTest,
    moduletag: :release,
    test_plan: "oidcc-client-basic-certification-test-plan",
    config: %{
      "alias" => "ueberauth_oidcc_basic_plain",
      "description" => "UeberauthOidcc (basic certification, plain_http_request)"
    },
    variant: %{
      "request_type" => "plain_http_request",
      "client_registration" => "dynamic_client"
    },
    overrides: %{
      "oidcc-client-test-nonce-invalid" => :login_fail,
      "oidcc-client-test-missing-iat" => :login_fail,
      "oidcc-client-test-invalid-aud" => :login_fail,
      "oidcc-client-test-missing-sub" => :login_fail,
      "oidcc-client-test-userinfo-invalid-sub" => :login_fail,
      "oidcc-client-test-invalid-sig-rs256" => :login_fail,
      "oidcc-client-test-invalid-iss" => :login_fail
    }
end
