defmodule OidcClientConfigTest do
  @moduledoc false
  use ExUnit.Case, async: true

  use UeberauthOidccCertification.DynamicConformanceTest,
    moduletag: :release,
    describe:
      "oidcc-client-config-certification-test-plan (client_secret_post, plain_http_request)",
    test_plan: "oidcc-client-config-certification-test-plan",
    config: %{
      "alias" => "ueberauth_oidcc_config_post_plain",
      "description" =>
        "UeberauthOidcc (config certification, client_secret_post, plain_http_request)"
    },
    variant: %{
      "client_auth_type" => "client_secret_post",
      "request_type" => "plain_http_request",
      "response_mode" => "default",
      "client_registration" => "dynamic_client"
    },
    default_task: :fetch_configuration,
    overrides: %{
      "oidcc-client-test-discovery-jwks-uri-keys" => :fetch_jwks,
      "oidcc-client-test-discovery-issuer-mismatch" => :fail_configuration,
      "oidcc-client-test-idtoken-sig-none" => :login,
      "oidcc-client-test-signing-key-rotation-just-before-signing" => :login,
      "oidcc-client-test-signing-key-rotation" => :login_twice
    }
end
