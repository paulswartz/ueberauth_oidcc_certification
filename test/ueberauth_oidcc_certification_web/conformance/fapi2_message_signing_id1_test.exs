defmodule OidccFapi2MessageSigningId1Test do
  @moduledoc false

  client_id = "ueberauth_oidcc"

  server_jwk =
    %{
      "kid" => "server_jwk",
      "alg" => "ES256",
      "crv" => "P-256",
      "d" => "E3cwJHhc5mdlIVMpxGMhGQJ5JjgF82jPY6_kU1w0eug",
      "kty" => "EC",
      "x" => "hXOsIznMqOJeAe2TAD-Z1DeTG1k5-2oehz7p4Kfb3lg",
      "y" => "CX52PvKg26GpZ3ldZx3QrzhzSXygA8a3Jch-bpZUudM"
    }

  server_jwk_rsa =
    %{
      "kid" => "server_jwk_rsa",
      "alg" => "PS256",
      "d" =>
        "QOVVfLv48j7m6JvUKuwFOLlL8VrGc8jEKgY4hfnMq2-0S_t1bD1sOj0hgqnlDsX6m9CS1tYGRpDrGKKHxdFfZIfs5Pyapap7oCkPCrNWB-ED7uYLMAMRpKYkoB_Idu-a33e-2aqpjDs_Mnpw63eGpC2s30hp7Y56DfdHcVkjwlkwdHqEaQePiSlcyOnQT_7nuvSuEPu3QYWdVBsxnqnMdDA5-IWC4IxCHNn5EQcLdYtUI4Eulx-UHA4imFrRaz0iaFarISUKVg9ChvA8fTJwJhroYK8P4izOR-0AOs8B5i6WdCuIfy9V6QGB2RM9E4VDURElAl3bIEc9LTOo1WNZsQ",
      "dp" =>
        "mEk7YRLTVLZ_9vIXzf83ftV17ChEEDGckmeeEyCEoojScHyxfhlDAHGJUqmqbvpjfsOTSF20jupCvFt95T0ZUNUiSeHGFO346R_8ff0dA5hZJ0kJWgR6GNcz8ap6PSL-l9zggragPG1uEoZCJ9TRZrqMI0s0CbrI_EsijjoA3yE",
      "dq" =>
        "b1A3QWKaWvUaXJlcCNpRX8frylUg0p0fgYjUnIoLgNuA-xqJdw9Bgi2VR17Q1PguMEPn5zlLT_xG7-8DPjifEllTs2lwinlW136pT_IYes6S4ERayunyNww5GIdNI_byzlnH4ANeebqN23YWt17lDWdpQugWQ2gBJ8Qwzc_LTFc",
      "e" => "AQAB",
      "kty" => "RSA",
      "n" =>
        "tTuSNy5ljS8qW84GWJ0LEzMTjM_aMlA2DOMrN4uRSkanG2UArbKWBhGzvCnG_Mx3S5rRaG7YNL7mDHyEMgow5xUhHMby7coSD5iTqFd-626g6dEgUVD3Ua00uB8eODEFiXY8yay_QtoMP1qS6BDSiOWS__FwbiOA6x5x-FZotQOii45tNEDQYcDttpfxlsDtHXJY3uHZszUvTbAR7wiLKITpNUZZ6E2DwGgnIGVh3DHfJiJSuIrilmjEIDkuaADfiRTnpXKKDaTgLNc2yT9HcOl9QDwrA3NHivflNDNvwtxPuCTzo1RtWKf5qDJmEo2C3jsFmvsa91NPG0xkK63LGw",
      "p" =>
        "-UwhrPywsRB9US-OBhtg1vCyPVnEr9x6CLKkQWuRXjOFVcVpscYSgQUqUgxwI6LB6oitfzhD6gvacTk-6hX2ZRnXWZALMeqTwoP-gOYsIITIEh-X3WeAzshvriO05lcREPE0ktBNeHIla7MKFBKGy2XiPRos0a7biETZbITzNJE",
      "q" =>
        "uhr2eA-YA5n4ww86NdDgGQjOpggNr9PrA63UqFrN4dWZ44J0vddlNCsgLvGgiBuorF--CJAp9krmp8nXZlEbj0gHPa12tZtr6dt_iyW-gI8I-OVhhPOvDQt5u2qWMKembqymLkxcbkW7osrY3t3n0zOusaIHnJBpiTATqFxg6us",
      "qi" =>
        "DdQvNwGW1R1x3Pbb59wkPgvNDB-eB5UtAKQhO_o_ijIJ6qQmasa2KyFyUGh0frGg47QvtVzl29VcMn0Fg8aQ8OOLS8-BIVifRUQwMxfkmAlJT1_hGF9plWuuC4pehNIguw7mhkjmTqSV7LziX2YwHcif5Xow_mcD6I8hKcFe2aA"
    }

  client_jwk = %{
    JOSE.JWK.from_pem(File.read!("priv/cert/selfsigned_key.pem"))
    | fields: %{"kid" => "client_jwk"}
  }

  client_jwk_public = client_jwk |> JOSE.JWK.to_public_map() |> elem(1)
  client_certificate_pem = File.read!("priv/cert/selfsigned.pem")

  use UeberauthOidccCertification.ConformanceTest,
    moduletag: :release,
    describe: "fapi2-message-signing-id1-client-test-plan (private_key_jwt, dpop, unsigned)",
    test_plan: "fapi2-message-signing-id1-client-test-plan",
    ueberauth_provider_configuration_opts: %{
      quirks: %{
        document_overrides: %{
          "code_challenge_methods_supported" => ["S256"],
          "subject_types_supported" => ["public"]
        }
      }
    },
    ueberauth_config: [
      client_id: client_id,
      client_secret: "",
      client_jwks: client_jwk,
      scopes: ~w(openid),
      profiles: ~w(fapi2_message_signing)a
    ],
    config: %{
      "alias" => "ueberauth_oidcc_fapi2_ms_unsigned",
      "server" => %{
        "jwks" => %{
          "keys" => [
            server_jwk
          ]
        }
      },
      "client" => %{
        "client_id" => client_id,
        "scope" => "openid",
        "redirect_uri" => "http://localhost:4000/auth/openid/callback",
        "jwks" => %{
          "keys" => [client_jwk_public]
        },
        "certificate" => client_certificate_pem
      },
      "description" => "UeberauthOidcc (FAPI2 message signing, private_key_jwt, dpop, unsigned)"
    },
    variant: %{
      "sender_constrain" => "dpop",
      "client_auth_type" => "private_key_jwt",
      "fapi_request_method" => "unsigned",
      "fapi_client_type" => "oidc",
      "fapi_profile" => "plain_fapi",
      "fapi_response_mode" => "plain_response"
    },
    default_task: :login_with_account,
    overrides: %{
      "fapi2-security-profile-id2-client-test-invalid-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-secondary-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-null-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-alternate-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-expired-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-authorization-response-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-missing-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-remove-authorization-response-iss" => :login_fail
    }

  use UeberauthOidccCertification.ConformanceTest,
    moduletag: :release,
    describe: "fapi2-message-signing-id1-client-test-plan (private_key_jwt, dpop)",
    test_plan: "fapi2-message-signing-id1-client-test-plan",
    ueberauth_provider_configuration_opts: %{
      quirks: %{
        document_overrides: %{
          "subject_types_supported" => ["public"],
          "code_challenge_methods_supported" => ["S256"],
          "request_parameter_supported" => true
        }
      }
    },
    ueberauth_config: [
      client_id: client_id,
      client_secret: "",
      client_jwks: client_jwk,
      scopes: ~w(openid),
      profiles: ~w(fapi2_message_signing)a
    ],
    config: %{
      "alias" => "ueberauth_oidcc_fapi2_ms_signed",
      "server" => %{
        "jwks" => %{
          "keys" => [
            server_jwk
          ]
        }
      },
      "client" => %{
        "client_id" => client_id,
        "scope" => "openid",
        "redirect_uri" => "http://localhost:4000/auth/openid/callback",
        "jwks" => %{
          "keys" => [client_jwk_public]
        },
        "certificate" => client_certificate_pem
      },
      "description" => "UeberauthOidcc (FAPI2 message signing, private_key_jwt, dpop)"
    },
    variant: %{
      "sender_constrain" => "dpop",
      "client_auth_type" => "private_key_jwt",
      "fapi_request_method" => "signed_non_repudiation",
      "fapi_client_type" => "oidc",
      "fapi_profile" => "plain_fapi",
      "fapi_response_mode" => "plain_response"
    },
    default_task: :login_with_account,
    overrides: %{
      "fapi2-security-profile-id2-client-test-invalid-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-secondary-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-null-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-alternate-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-expired-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-authorization-response-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-missing-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-remove-authorization-response-iss" => :login_fail
    }

  use UeberauthOidccCertification.ConformanceTest,
    moduletag: :release,
    describe: "fapi2-message-signing-id1-client-test-plan (private_key_jwt, dpop, jarm)",
    test_plan: "fapi2-message-signing-id1-client-test-plan",
    ueberauth_provider_configuration_opts: %{
      quirks: %{
        document_overrides: %{
          "subject_types_supported" => ["public"],
          "code_challenge_methods_supported" => ["S256"],
          "request_parameter_supported" => true
        }
      }
    },
    ueberauth_config: [
      client_id: client_id,
      client_secret: "",
      client_jwks: client_jwk,
      scopes: ~w(openid),
      profiles: ~w(fapi2_message_signing)a
    ],
    config: %{
      "alias" => "ueberauth_oidcc_fapi2_ms_jarm",
      "server" => %{
        "jwks" => %{
          "keys" => [
            server_jwk
          ]
        }
      },
      "client" => %{
        "client_id" => client_id,
        "scope" => "openid",
        "redirect_uri" => "http://localhost:4000/auth/openid/callback",
        "jwks" => %{
          "keys" => [client_jwk_public]
        },
        "certificate" => client_certificate_pem
      },
      "description" => "UeberauthOidcc (FAPI2 profile, private_key_jwt, dpop, jarm)"
    },
    variant: %{
      "sender_constrain" => "dpop",
      "client_auth_type" => "private_key_jwt",
      "fapi_request_method" => "signed_non_repudiation",
      "fapi_client_type" => "oidc",
      "fapi_profile" => "plain_fapi",
      "fapi_response_mode" => "jarm"
    },
    default_task: :login_with_account,
    overrides: %{
      "fapi2-security-profile-id2-client-test-invalid-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-secondary-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-null-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-alternate-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-expired-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-authorization-response-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-missing-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-remove-authorization-response-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-iss-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-iss-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-aud-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-aud-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-exp-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-expired-exp-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-sig-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-signature-is-not-none" => :login_fail
    }

  use UeberauthOidccCertification.ConformanceTest,
    moduletag: :release,
    describe:
      "fapi2-message-signing-id1-client-test-plan (private_key_jwt, dpop, jarm, encrypted)",
    test_plan: "fapi2-message-signing-id1-client-test-plan",
    ueberauth_provider_configuration_opts: %{
      quirks: %{
        document_overrides: %{
          "subject_types_supported" => ["public"],
          "code_challenge_methods_supported" => ["S256"],
          "request_parameter_supported" => true,
          "request_object_encryption_alg_values_supported" => ["ECDH-ES+A256KW"],
          "request_object_encryption_enc_values_supported" => ["A256GCM"],
          "authorization_encryption_alg_values_supported" => ["ECDH-ES+A256KW"],
          "authorization_encryption_enc_values_supported" => ["A256GCM"]
        }
      }
    },
    ueberauth_config: [
      client_id: client_id,
      client_secret: "",
      client_jwks: client_jwk,
      scopes: ~w(openid),
      profiles: ~w(fapi2_message_signing)a
    ],
    config: %{
      "alias" => "ueberauth_oidcc_fapi2_ms_jarm_encrypted",
      "server" => %{
        "jwks" => %{
          "keys" => [
            server_jwk
          ]
        }
      },
      "client" => %{
        "client_id" => client_id,
        "scope" => "openid",
        "redirect_uri" => "http://localhost:4000/auth/openid/callback",
        "jwks" => %{
          "keys" => [client_jwk_public]
        },
        "request_object_encryption_alg" => "ECDH-ES+A256KW",
        "request_object_encryption_enc" => "A256GCM",
        "authorization_encrypted_response_alg" => "ECDH-ES+A256KW",
        "authorization_encrypted_response_enc" => "A256GCM",
        "certificate" => client_certificate_pem
      },
      "description" => "UeberauthOidcc (FAPI2 profile, private_key_jwt, dpop, jarm, encrypted)"
    },
    variant: %{
      "sender_constrain" => "dpop",
      "client_auth_type" => "private_key_jwt",
      "fapi_request_method" => "signed_non_repudiation",
      "fapi_client_type" => "oidc",
      "fapi_profile" => "plain_fapi",
      "fapi_response_mode" => "jarm"
    },
    default_task: :login_with_account,
    overrides: %{
      "fapi2-security-profile-id2-client-test-invalid-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-secondary-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-null-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-alternate-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-expired-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-authorization-response-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-missing-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-remove-authorization-response-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-iss-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-iss-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-aud-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-aud-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-exp-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-expired-exp-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-sig-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-signature-is-not-none" => :login_fail
    }

  use UeberauthOidccCertification.ConformanceTest,
    moduletag: :release,
    describe: "fapi2-message-signing-id1-client-test-plan (mtls, jarm)",
    test_plan: "fapi2-message-signing-id1-client-test-plan",
    ueberauth_provider_configuration_opts: %{
      quirks: %{
        document_overrides: %{
          "subject_types_supported" => ["public"],
          "code_challenge_methods_supported" => ["S256"],
          "request_parameter_supported" => true
        }
      }
    },
    ueberauth_config: [
      client_id: client_id,
      client_secret: "",
      client_jwks: client_jwk,
      scopes: ~w(openid),
      profiles: ~w(fapi2_message_signing)a,
      request_opts: %{
        httpc_profile: :fapi2_message_signing,
        ssl: [
          certfile: "priv/cert/selfsigned.pem",
          keyfile: "priv/cert/selfsigned_key.pem"
        ]
      }
    ],
    config: %{
      "alias" => "ueberauth_oidcc_fapi2_ms_mtls_jarm",
      "server" => %{
        "jwks" => %{
          "keys" => [
            server_jwk
          ]
        }
      },
      "client" => %{
        "client_id" => client_id,
        "scope" => "openid",
        # "redirect_uri" is set during generation
        "jwks" => %{
          "keys" => [client_jwk_public]
        },
        "certificate" => client_certificate_pem
      },
      "description" => "UeberauthOidcc (FAPI2 message signing, tls_client_auth, jarm)"
    },
    variant: %{
      "sender_constrain" => "mtls",
      "client_auth_type" => "mtls",
      "fapi_request_method" => "signed_non_repudiation",
      "fapi_client_type" => "oidc",
      "fapi_profile" => "plain_fapi",
      "fapi_response_mode" => "jarm"
    },
    default_task: :login_with_account,
    overrides: %{
      "fapi2-security-profile-id2-client-test-invalid-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-secondary-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-null-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-alternate-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-expired-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-authorization-response-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-missing-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-remove-authorization-response-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-iss-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-iss-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-aud-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-aud-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-exp-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-expired-exp-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-sig-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-signature-is-not-none" => :login_fail
    }

  use UeberauthOidccCertification.ConformanceTest,
    moduletag: :release,
    describe: "fapi2-message-signing-id1-client-test-plan (mtls, dpop, jarm)",
    test_plan: "fapi2-message-signing-id1-client-test-plan",
    ueberauth_provider_configuration_opts: %{
      quirks: %{
        document_overrides: %{
          "subject_types_supported" => ["public"],
          "code_challenge_methods_supported" => ["S256"],
          "request_parameter_supported" => true
        }
      }
    },
    ueberauth_config: [
      client_id: client_id,
      client_secret: "",
      client_jwks: client_jwk,
      scopes: ~w(openid),
      profiles: ~w(fapi2_message_signing)a,
      request_opts: %{
        httpc_profile: :fapi2_message_signing,
        ssl: [
          certfile: "priv/cert/selfsigned.pem",
          keyfile: "priv/cert/selfsigned_key.pem"
        ]
      }
    ],
    config: %{
      "alias" => "ueberauth_oidcc_fapi2_ms_mtls_dpop_jarm",
      "server" => %{
        "jwks" => %{
          "keys" => [
            server_jwk
          ]
        }
      },
      "client" => %{
        "client_id" => client_id,
        "scope" => "openid",
        # "redirect_uri" is set during generation
        "jwks" => %{
          "keys" => [client_jwk_public]
        },
        "certificate" => client_certificate_pem
      },
      "description" => "UeberauthOidcc (FAPI2 message signing, tls_client_auth, dpop, jarm)"
    },
    variant: %{
      "sender_constrain" => "dpop",
      "client_auth_type" => "mtls",
      "fapi_request_method" => "signed_non_repudiation",
      "fapi_client_type" => "oidc",
      "fapi_profile" => "plain_fapi",
      "fapi_response_mode" => "jarm"
    },
    default_task: :login_with_account,
    overrides: %{
      "fapi2-security-profile-id2-client-test-invalid-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-secondary-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-null-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-alternate-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-expired-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-authorization-response-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-missing-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-remove-authorization-response-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-iss-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-iss-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-aud-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-aud-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-exp-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-expired-exp-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-sig-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-signature-is-not-none" => :login_fail
    }

  use UeberauthOidccCertification.ConformanceTest,
    # moduletag: :connectid_au,
    moduletag: :skip,
    describe: "fapi2-message-signing-id1-client-test-plan (connectid_au)",
    test_plan: "fapi2-message-signing-id1-client-test-plan",
    ueberauth_provider_configuration_opts: %{
      quirks: %{
        document_overrides: %{
          "subject_types_supported" => ["public"],
          "code_challenge_methods_supported" => ["S256"],
          "request_parameter_supported" => true
        }
      }
    },
    ueberauth_config: [
      client_id: client_id,
      client_secret: "",
      client_jwks: client_jwk,
      scopes: ~w(openid),
      profiles: ~w(fapi2_connectid_au)a,
      purpose: "OpenID Certification",
      request_opts: %{
        httpc_profile: :fapi2_message_signing,
        ssl: [
          certfile: "priv/cert/selfsigned.pem",
          keyfile: "priv/cert/selfsigned_key.pem"
        ]
      }
    ],
    config: %{
      "alias" => "ueberauth_oidcc_fapi2_ms_jarm_connectid",
      "server" => %{
        "jwks" => %{
          "keys" => [
            server_jwk_rsa
          ]
        }
      },
      "client" => %{
        "client_id" => client_id,
        "scope" => "openid",
        "jwks" => %{
          "keys" => [client_jwk_public]
        },
        "certificate" => client_certificate_pem
      },
      "description" => "UeberauthOidcc (FAPI2 profile, connectid_au)"
    },
    variant: %{
      "sender_constrain" => "mtls",
      "client_auth_type" => "private_key_jwt",
      "fapi_request_method" => "signed_non_repudiation",
      "fapi_client_type" => "oidc",
      "fapi_profile" => "connectid_au",
      "fapi_response_mode" => "plain_response"
    },
    default_task: :login,
    overrides: %{
      "fapi2-security-profile-id2-client-test-invalid-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-secondary-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-null-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-alternate-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-expired-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-authorization-response-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-missing-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-remove-authorization-response-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-iss-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-iss-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-aud-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-aud-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-exp-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-expired-exp-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-sig-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-signature-is-not-none" => :login_fail
    }

  use UeberauthOidccCertification.ConformanceTest,
    # moduletag: :openbanking_uk,
    moduletag: :skip,
    describe: "fapi2-message-signing-id1-client-test-plan (openbanking_uk)",
    test_plan: "fapi2-message-signing-id1-client-test-plan",
    ueberauth_provider_configuration_opts: %{
      quirks: %{
        document_overrides: %{
          "subject_types_supported" => ["public"],
          "code_challenge_methods_supported" => ["S256"],
          "request_parameter_supported" => true
        }
      }
    },
    ueberauth_config: [
      client_id: client_id,
      client_secret: "",
      client_jwks: client_jwk,
      scopes: ~w(openid),
      profiles: ~w(fapi2_connectid_au)a,
      request_opts: %{
        httpc_profile: :fapi2_message_signing,
        ssl: [
          certfile: "priv/cert/selfsigned.pem",
          keyfile: "priv/cert/selfsigned_key.pem"
        ]
      }
    ],
    config: %{
      "alias" => "ueberauth_oidcc_fapi2_ms_jarm_openbanking",
      "server" => %{
        "jwks" => %{
          "keys" => [
            server_jwk
          ]
        }
      },
      "client" => %{
        "client_id" => client_id,
        "scope" => "openid",
        "jwks" => %{
          "keys" => [client_jwk_public]
        },
        "certificate" => client_certificate_pem
      },
      "description" => "UeberauthOidcc (FAPI2 profile, openbanking_uk)"
    },
    variant: %{
      "sender_constrain" => "dpop",
      "client_auth_type" => "private_key_jwt",
      "fapi_request_method" => "signed_non_repudiation",
      "fapi_client_type" => "oidc",
      "fapi_profile" => "openbanking_uk",
      "fapi_response_mode" => "plain_response"
    },
    default_task: :login,
    overrides: %{
      "fapi2-security-profile-id2-client-test-invalid-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-secondary-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-null-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-alternate-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-expired-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-authorization-response-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-missing-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-remove-authorization-response-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-iss-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-iss-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-aud-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-aud-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-exp-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-expired-exp-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-sig-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-signature-is-not-none" => :login_fail
    }

  use UeberauthOidccCertification.ConformanceTest,
    # moduletag: :consumerdataright_au,
    moduletag: :skip,
    describe: "fapi2-message-signing-id1-client-test-plan (consumerdataright_au)",
    test_plan: "fapi2-message-signing-id1-client-test-plan",
    ueberauth_provider_configuration_opts: %{
      quirks: %{
        document_overrides: %{
          "subject_types_supported" => ["public"],
          "code_challenge_methods_supported" => ["S256"],
          "request_parameter_supported" => true
        }
      }
    },
    ueberauth_config: [
      client_id: client_id,
      client_secret: "",
      client_jwks: client_jwk,
      scopes: ~w(openid),
      profiles: ~w(fapi2_connectid_au)a,
      request_opts: %{
        httpc_profile: :fapi2_message_signing,
        ssl: [
          certfile: "priv/cert/selfsigned.pem",
          keyfile: "priv/cert/selfsigned_key.pem"
        ]
      }
    ],
    config: %{
      "alias" => "ueberauth_oidcc_fapi2_ms_jarm_consumerdataright",
      "server" => %{
        "jwks" => %{
          "keys" => [
            server_jwk
          ]
        }
      },
      "client" => %{
        "client_id" => client_id,
        "scope" => "openid",
        "jwks" => %{
          "keys" => [client_jwk_public]
        },
        "certificate" => client_certificate_pem
      },
      "description" => "UeberauthOidcc (FAPI2 profile, consumerdataright_au)"
    },
    variant: %{
      "sender_constrain" => "mtls",
      "client_auth_type" => "private_key_jwt",
      "fapi_request_method" => "signed_non_repudiation",
      "fapi_client_type" => "oidc",
      "fapi_profile" => "consumerdataright_au",
      "fapi_response_mode" => "plain_response"
    },
    default_task: :login_with_account,
    overrides: %{
      "fapi2-security-profile-id2-client-test-invalid-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-secondary-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-null-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-alternate-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-expired-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-authorization-response-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-missing-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-remove-authorization-response-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-iss-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-iss-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-aud-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-aud-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-without-exp-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-expired-exp-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-with-invalid-sig-fails" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-jarm-signature-is-not-none" => :login_fail
    }
end
