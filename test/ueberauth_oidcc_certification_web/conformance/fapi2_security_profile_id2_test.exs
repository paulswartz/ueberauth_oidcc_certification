defmodule OidccFapi2SecurityProfileId2Test do
  @moduledoc false

  client_id = "ueberauth_oidcc"

  server_jwk =
    %{
      "kid" => "server_jwk",
      "use" => "sig",
      "alg" => "ES256",
      "crv" => "P-256",
      "d" => "E3cwJHhc5mdlIVMpxGMhGQJ5JjgF82jPY6_kU1w0eug",
      "kty" => "EC",
      "x" => "hXOsIznMqOJeAe2TAD-Z1DeTG1k5-2oehz7p4Kfb3lg",
      "y" => "CX52PvKg26GpZ3ldZx3QrzhzSXygA8a3Jch-bpZUudM"
    }

  client_jwk = %{
    JOSE.JWK.from_pem(File.read!("priv/cert/selfsigned_key.pem"))
    | fields: %{"kid" => "client_jwk", "use" => "sig"}
  }

  client_public_jwk = client_jwk |> JOSE.JWK.to_public_map() |> elem(1)
  client_certificate_pem = File.read!("priv/cert/selfsigned.pem")

  use UeberauthOidccCertification.ConformanceTest,
    moduletag: :release,
    describe: "fapi2-security-profile-id2-client-test-plan (private_key_jwt, dpop)",
    test_plan: "fapi2-security-profile-id2-client-test-plan",
    ueberauth_provider_configuration_opts: %{
      quirks: %{
        document_overrides: %{
          "code_challenge_methods_supported" => ["S256"],
          "subject_types_supported" => ["public"]
        }
      }
    },
    ueberauth_config: [
      client_id: client_id,
      client_secret: "",
      client_jwks: client_jwk,
      scopes: ~w(openid),
      profiles: ~w(fapi2_security_profile)a
    ],
    config: %{
      "alias" => "ueberauth_oidcc_fapi2",
      "server" => %{
        "jwks" => %{
          "keys" => [
            server_jwk
          ]
        }
      },
      "client" => %{
        "client_id" => client_id,
        "scope" => "openid",
        "redirect_uri" => "http://localhost:4000/auth/openid/callback",
        "jwks" => %{
          "keys" => [client_public_jwk]
        },
        "certificate" => client_certificate_pem
      },
      "description" => "UeberauthOidcc (FAPI2 security profile, private_key_jwt, dpop)"
    },
    variant: %{
      "sender_constrain" => "dpop",
      "client_auth_type" => "private_key_jwt",
      "fapi_client_type" => "oidc",
      "fapi_profile" => "plain_fapi"
    },
    default_task: :login_with_account,
    overrides: %{
      "fapi2-security-profile-id2-client-test-invalid-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-secondary-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-null-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-alternate-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-expired-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-authorization-response-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-missing-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-remove-authorization-response-iss" => :login_fail
    }

  use UeberauthOidccCertification.ConformanceTest,
    moduletag: :release,
    describe: "fapi2-security-profile-id2-client-test-plan (mtls)",
    test_plan: "fapi2-security-profile-id2-client-test-plan",
    ueberauth_provider_configuration_opts: %{
      quirks: %{
        document_overrides: %{
          "code_challenge_methods_supported" => ["S256"],
          "subject_types_supported" => ["public"]
        }
      }
    },
    ueberauth_config: [
      client_id: client_id,
      client_secret: "",
      client_jwks: client_jwk,
      scopes: ~w(openid),
      profiles: ~w(fapi2_security_profile)a,
      request_opts: %{
        httpc_profile: :fapi2_security_profile,
        ssl: [
          verify: :verify_peer,
          cacerts: :public_key.cacerts_get(),
          certfile: "priv/cert/selfsigned.pem",
          keyfile: "priv/cert/selfsigned_key.pem"
        ]
      }
    ],
    config: %{
      "alias" => "ueberauth_oidcc_fapi2_sp_mtls",
      "server" => %{
        "jwks" => %{
          "keys" => [
            server_jwk
          ]
        }
      },
      "client" => %{
        "client_id" => client_id,
        "scope" => "openid",
        "redirect_uri" => "http://localhost:4000/auth/openid/callback",
        "jwks" => %{
          "keys" => [
            client_public_jwk
          ]
        },
        "self_signed_tls_client_auth" => true,
        "tls_client_auth_subject_dn" => "CN=Self-signed test certificate,O=Phoenix Framework",
        "certificate" => client_certificate_pem
      },
      "description" => "UeberauthOidcc (FAPI2 security profile, mtls)"
    },
    variant: %{
      "sender_constrain" => "mtls",
      "client_auth_type" => "mtls",
      "fapi_client_type" => "oidc",
      "fapi_profile" => "plain_fapi"
    },
    default_task: :login_with_account,
    overrides: %{
      "fapi2-security-profile-id2-client-test-invalid-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-secondary-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-null-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-alternate-alg" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-aud" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-expired-exp" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-missing-nonce" => :login_fail,
      "fapi2-security-profile-id2-client-test-invalid-authorization-response-iss" => :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-missing-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-ensure-authorization-response-with-invalid-state-fails" =>
        :login_fail,
      "fapi2-security-profile-id2-client-test-remove-authorization-response-iss" => :login_fail
    }
end
