defmodule OidcClientTestPlanTest do
  @moduledoc false
  use ExUnit.Case

  client_jwk =
    JOSE.JWK.from_map(%{
      # kid is required for `oidcc-client-test`
      "kid" => "36c91fb4-f6f8-4d8d-b638-64199d9d466c",
      "use" => "sig",
      "crv" => "P-256",
      "d" => "moyQLBd3-EKeAnC04YcuXFBO2cgGno3S_jz3X07FQXc",
      "kty" => "EC",
      "x" => "X12bvyomHaJF2gf7NFNwFrxpDdDGpm-dYOrZPmzucEo",
      "y" => "FDsDpY6Dtdg0aGyrkaSEhdCrWCWyXt2ukuVb7HW8hEY"
    })

  use UeberauthOidccCertification.DynamicConformanceTest,
    moduletag: :release,
    describe: "oidcc-client-test-plan (private_key_jwt, request_object)",
    test_plan: "oidcc-client-test-plan",
    ueberauth_config: [
      client_jwks: client_jwk
    ],
    config: %{
      "alias" => "ueberauth_oidcc_post_plain",
      "description" => "UeberauthOidcc (client test plan, private_key_jwt, request_object)"
    },
    variant: %{
      "client_auth_type" => "private_key_jwt",
      "request_type" => "request_object",
      "response_mode" => "default",
      "response_type" => "code",
      "client_registration" => "dynamic_client"
    },
    overrides: %{
      "oidcc-client-test-client-secret-basic" => :skip,
      "oidcc-client-test-nonce-invalid" => :login_fail,
      "oidcc-client-test-missing-iat" => :login_fail,
      "oidcc-client-test-missing-aud" => :login_fail,
      "oidcc-client-test-invalid-aud" => :login_fail,
      "oidcc-client-test-missing-sub" => :login_fail,
      "oidcc-client-test-invalid-sig-rs256" => :login_fail,
      "oidcc-client-test-invalid-iss" => :login_fail,
      "oidcc-client-test-userinfo-invalid-sub" => :login_fail,
      "oidcc-client-test-userinfo-bearer-body" => :skip,
      "oidcc-client-test-invalid-sig-hs256" => :login_fail,
      "oidcc-client-test-invalid-sig-es256" => :login_fail,
      "oidcc-client-test-aggregated-claims" => :skip,
      "oidcc-client-test-discovery-openid-config" => :fetch_configuration,
      "oidcc-client-test-discovery-jwks-uri-keys" => :fetch_jwks,
      "oidcc-client-test-discovery-issuer-mismatch" => :fail_configuration,
      "oidcc-client-test-signing-key-rotation" => :skip,
      "oidcc-client-test-discovery-webfinger-acct" => :skip,
      "oidcc-client-test-discovery-webfinger-url" => :skip,
      "oidcc-client-test-dynamic-registration" => :register
    }

  use UeberauthOidccCertification.DynamicConformanceTest,
    moduletag: :release,
    describe: "oidcc-client-test-plan (client_secret_post, plain_http_request)",
    test_plan: "oidcc-client-test-plan",
    config: %{
      "alias" => "ueberauth_oidcc_post",
      "description" => "UeberauthOidcc (client test plan, client_secret_post, plain_http_request)"
    },
    variant: %{
      "client_auth_type" => "client_secret_post",
      "request_type" => "plain_http_request",
      "response_mode" => "default",
      "response_type" => "code",
      "client_registration" => "dynamic_client"
    },
    overrides: %{
      "oidcc-client-test-nonce-invalid" => :login_fail,
      "oidcc-client-test-missing-iat" => :login_fail,
      "oidcc-client-test-missing-aud" => :login_fail,
      "oidcc-client-test-invalid-aud" => :login_fail,
      "oidcc-client-test-missing-sub" => :login_fail,
      "oidcc-client-test-invalid-sig-rs256" => :login_fail,
      "oidcc-client-test-invalid-iss" => :login_fail,
      "oidcc-client-test-userinfo-invalid-sub" => :login_fail,
      "oidcc-client-test-userinfo-bearer-body" => :skip,
      "oidcc-client-test-invalid-sig-hs256" => :login_fail,
      "oidcc-client-test-invalid-sig-es256" => :login_fail,
      "oidcc-client-test-aggregated-claims" => :skip,
      "oidcc-client-test-discovery-openid-config" => :fetch_configuration,
      "oidcc-client-test-discovery-jwks-uri-keys" => :fetch_jwks,
      "oidcc-client-test-discovery-issuer-mismatch" => :fail_configuration,
      "oidcc-client-test-signing-key-rotation" => :login_twice,
      "oidcc-client-test-discovery-webfinger-acct" => :skip,
      "oidcc-client-test-discovery-webfinger-url" => :skip,
      "oidcc-client-test-dynamic-registration" => :register
    }

  use UeberauthOidccCertification.DynamicConformanceTest,
    describe: "oidcc-client-test-plan (client_secret_jwt, plain_http_request)",
    test_plan: "oidcc-client-test-plan",
    config: %{
      "alias" => "ueberauth_oidcc_jwt_plain",
      "description" => "UeberauthOidcc (client_secret_jwt, plain_http_request)"
    },
    variant: %{
      "client_auth_type" => "client_secret_jwt",
      "request_type" => "plain_http_request",
      "response_mode" => "default",
      "response_type" => "code",
      "client_registration" => "dynamic_client"
    },
    overrides: %{
      "oidcc-client-test-nonce-invalid" => :login_fail,
      "oidcc-client-test-missing-iat" => :login_fail,
      "oidcc-client-test-missing-aud" => :login_fail,
      "oidcc-client-test-invalid-aud" => :login_fail,
      "oidcc-client-test-missing-sub" => :login_fail,
      "oidcc-client-test-invalid-sig-rs256" => :login_fail,
      "oidcc-client-test-invalid-iss" => :login_fail,
      "oidcc-client-test-userinfo-invalid-sub" => :login_fail,
      "oidcc-client-test-userinfo-bearer-body" => :skip,
      "oidcc-client-test-invalid-sig-hs256" => :login_fail,
      "oidcc-client-test-invalid-sig-es256" => :login_fail,
      "oidcc-client-test-aggregated-claims" => :skip,
      "oidcc-client-test-discovery-openid-config" => :fetch_configuration,
      "oidcc-client-test-discovery-jwks-uri-keys" => :fetch_jwks,
      "oidcc-client-test-discovery-issuer-mismatch" => :fail_configuration,
      "oidcc-client-test-signing-key-rotation" => :login_twice,
      "oidcc-client-test-discovery-webfinger-acct" => :skip,
      "oidcc-client-test-discovery-webfinger-url" => :skip,
      "oidcc-client-test-dynamic-registration" => :register
    }

  # use UeberauthOidccCertification.ConformanceTest,
  #   describe: "oidcc-client-test-plan (client_secret_jwt, request_object)",
  #   test_plan: "oidcc-client-test-plan",
  #   ueberauth_provider_configuration_opts: %{
  #     quirks: %{
  #       document_overrides: %{
  #         "request_object_signing_alg_values_supported" => ["HS256"],
  #         "token_endpoint_auth_signing_alg_values_supported" => ["HS256"]
  #       }
  #     }
  #   },
  #   ueberauth_config: [
  #     client_id: client_id,
  #     client_secret: client_secret
  #   ],
  #   config: %{
  #     "alias" => "ueberauth_oidcc_jwt_object",
  #     "client" => %{
  #       "client_id" => client_id,
  #       "client_secret" => client_secret,
  #       "client_secret_jwt_alg" => "HS256",
  #       "redirect_uri" => "http://localhost:4000/auth/openid/callback"
  #     },
  #     "description" => "UeberauthOidcc"
  #   },
  #   variant: %{
  #     "client_auth_type" => "client_secret_jwt",
  #     "client_registration" => "static_client",
  #     "request_type" => "request_object",
  #     "response_mode" => "default",
  #     "response_type" => "code",
  #     "client_registration" => "dynamic_client"
  #   },
  #   overrides: %{
  #     "oidcc-client-test-nonce-invalid" => :login_fail,
  #     "oidcc-client-test-missing-iat" => :login_fail,
  #     "oidcc-client-test-missing-aud" => :login_fail,
  #     "oidcc-client-test-invalid-aud" => :login_fail,
  #     "oidcc-client-test-missing-sub" => :login_fail,
  #     "oidcc-client-test-invalid-sig-rs256" => :login_fail,
  #     "oidcc-client-test-invalid-iss" => :login_fail,
  #     "oidcc-client-test-userinfo-invalid-sub" => :login_fail,
  #     "oidcc-client-test-userinfo-bearer-body" => :skip,
  #     "oidcc-client-test-invalid-sig-hs256" => :login_fail,
  #     "oidcc-client-test-invalid-sig-es256" => :login_fail,
  #     "oidcc-client-test-aggregated-claims" => :skip,
  #     "oidcc-client-test-discovery-openid-config" => :restart,
  #     "oidcc-client-test-discovery-jwks-uri-keys" => :restart,
  #     "oidcc-client-test-discovery-issuer-mismatch" => :restart,
  #     "oidcc-client-test-signing-key-rotation" => :login_twice,
  #     "oidcc-client-test-discovery-webfinger-acct" => :skip,
  #     "oidcc-client-test-discovery-webfinger-url" => :skip
  #   }
end
