defmodule OidcRpInitiatedLogoutTest do
  @moduledoc false
  use ExUnit.Case

  use UeberauthOidccCertification.DynamicConformanceTest,
    moduletag: :release,
    describe: "oidcc-client-rp-initiated-logout-rp-basic (client_secret_basic, plain, default)",
    test_plan: "oidcc-client-rp-initiated-logout-rp-basic",
    config: %{
      "alias" => "ueberauth_oidcc_logout_basic_plain",
      "description" => "UeberauthOidcc (RP initiated logout, client_secret_basic, plain, default)"
    },
    variant: %{
      "client_auth_type" => "client_secret_basic",
      "request_type" => "plain_http_request",
      "response_mode" => "default",
      "client_registration" => "dynamic_client"
    },
    default_task: :login_logout

  use UeberauthOidccCertification.DynamicConformanceTest,
    describe: "oidcc-client-rp-initiated-logout-rp-basic (client_secret_post, plain, form_post)",
    test_plan: "oidcc-client-rp-initiated-logout-rp-basic",
    ueberauth_config: [
      callback_methods: ["POST"],
      session_same_site: "None"
    ],
    config: %{
      "alias" => "ueberauth_oidcc_logout_post_plain_post",
      "description" =>
        "UeberauthOidcc (RP initiated logout, client_secret_post, plain, form_post)"
    },
    variant: %{
      "client_auth_type" => "client_secret_post",
      "request_type" => "plain_http_request",
      "response_mode" => "form_post",
      "client_registration" => "dynamic_client"
    },
    default_task: :login_logout
end
