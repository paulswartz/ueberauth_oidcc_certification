defmodule UeberauthOidccCertification do
  @moduledoc """
  UeberauthOidccCertification keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  def version do
    "#{ueberauth_oidcc_version()}, oidcc #{oidcc_version()}"
  end

  def unique_suffix do
    "_#{:erlang.phash2(version())}"
  end

  def ueberauth_oidcc_version do
    version_from(:ueberauth_oidcc)
  end

  def oidcc_version do
    version_from(:oidcc)
  end

  defp version_from(app) do
    envvar = String.upcase(Atom.to_string(app))
    path = System.get_env("#{envvar}_PATH", "")
    ref = System.get_env("#{envvar}_REF", "")
    :application.load(app)
    {:ok, vsn_charlist} = :application.get_key(app, :vsn)

    cond do
      path != "" ->
        "path: #{path}"

      ref != "" and ref != "release" ->
        "ref: #{ref}"

      true ->
        IO.iodata_to_binary(vsn_charlist)
    end
  end
end
