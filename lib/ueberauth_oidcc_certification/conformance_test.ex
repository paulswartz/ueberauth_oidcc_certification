defmodule UeberauthOidccCertification.ConformanceTest do
  alias UeberauthOidccCertification.ConformanceClient, as: CC
  use Wallaby.DSL
  import Kernel, except: [inspect: 1]
  import ExUnit.Assertions
  import ExUnit.Callbacks
  alias Phoenix.PubSub

  @pubsub UeberauthOidccCertification.PubSub

  defmacro __using__(opts) do
    quote bind_quoted: [opts: opts] do
      defmodule String.to_atom(opts[:describe] || opts[:test_plan]) do
        @moduledoc false
        use ExUnit.Case, async: true
        if opts[:moduletag], do: @moduletag(opts[:moduletag])

        alias UeberauthOidccCertification.ConformanceTest, as: CT
        require Logger

        alias UeberauthOidccCertification.ConformanceClient, as: CC

        @test_plan opts[:test_plan]
        @ueberauth_provider_configuration_opts opts[:ueberauth_provider_configuration_opts] || %{}
        @ueberauth_config opts[:ueberauth_config]
        @provider String.to_atom(
                    Map.fetch!(opts[:config], "alias") <>
                      UeberauthOidccCertification.unique_suffix()
                  )
        @config opts[:config]
                |> Map.put("alias", Atom.to_string(@provider))
                |> Map.update!("description", fn d ->
                  "#{d} (#{UeberauthOidccCertification.version()})"
                end)
                |> Map.update!("client", &CT.update_client_redirect_uri(&1, @provider))

        @variant opts[:variant]
        @default_task opts[:default_task] || :login
        @overrides opts[:overrides] || %{}

        test_plan_info =
          if opts[:moduletag] == :skip do
            %{"modules" => []}
          else
            CC.find_or_create_test_plan(
              CC.new(),
              @test_plan,
              @variant,
              @config
            )
          end

        def test_plan, do: @test_plan
        def test_plan_info, do: unquote(Macro.escape(test_plan_info))
        def ueberauth_provider_configuration_opts, do: @ueberauth_provider_configuration_opts
        def ueberauth_config, do: @ueberauth_config
        def provider, do: @provider
        def config, do: @config
        def variant, do: @variant

        setup_all do
          CT.setup_all(__MODULE__)
        end

        setup do
          CT.setup(__MODULE__)
        end

        describe "(#{test_plan_info["id"]})" do
          for module <- test_plan_info["modules"],
              test_name = module["testModule"],
              task = Map.get(@overrides, test_name, @default_task) do
            if task == :skip, do: @tag(:skip)
            @tag String.to_atom(test_name)
            test test_name, tags do
              CT.run_test_case(__MODULE__, unquote(test_name), unquote(task), tags)
            end
          end
        end
      end
    end
  end

  def setup_all(mod) do
    client = CC.new()

    if httpc_profile = mod.ueberauth_config()[:request_opts][:httpc_profile] do
      :inets.start(:httpc, profile: httpc_profile)
    end

    base_url =
      Application.get_env(
        :ueberauth_oidcc_certification,
        :base_url,
        "https://www.certification.openid.net"
      )

    test_plan_name = mod.test_plan_info()["planName"]
    test_plan_id = mod.test_plan_info()["id"]
    zip_path = "#{test_plan_name}-#{test_plan_id}"
    root = Path.join("artifacts", zip_path)
    File.mkdir_p!(Path.join(root, "client-data"))

    notes =
      for {k, v} <- [
            {"OpenID Conformance Profile",
             mod.test_plan_info()["certificationProfileName"] || "unknown"},
            {"Conformance Test Suite Software",
             "#{String.replace_leading(base_url, "https://", "")} version #{mod.test_plan_info()["version"]}"},
            {"Software Name & Version #",
             "ueberauth_oidcc #{UeberauthOidccCertification.version()}"},
            {"Test Date", Date.to_iso8601(Date.utc_today())}
          ] do
        [k, ": ", v, "\n"]
      end

    File.write!(Path.join(root, "NOTES.txt"), notes)

    on_exit(fn ->
      %{status: 200, body: body} = CC.test_log(client, test_plan_id)
      File.write!(Path.join(root, "#{zip_path}.zip"), body)
      :ok
    end)

    {:ok, %{client: client, base_url: base_url, root: root}}
  end

  def setup(mod) do
    CC.with_config_lock(fn ->
      ueberauth_config = Application.get_env(:ueberauth, Ueberauth)

      providers =
        Keyword.put(
          ueberauth_config[:providers],
          mod.provider(),
          {Ueberauth.Strategy.Oidcc,
           Keyword.merge(
             [
               issuer: mod.provider(),
               userinfo: true,
               scopes: ~w(openid profile),
               callback_methods: ["GET"],
               client_id: mod.config()["client"]["client_id"],
               client_secret:
                 case mod.config()["client"]["client_secret"] do
                   nil -> Base.url_encode64(:crypto.strong_rand_bytes(32), padding: false)
                   secret -> secret
                 end
             ],
             mod.ueberauth_config()
           )}
        )

      Application.put_env(
        :ueberauth,
        Ueberauth,
        Keyword.put(ueberauth_config, :providers, providers)
      )
    end)

    :ok
  end

  def run_test_case(mod, test_name, task, tags) do
    %{client: client, base_url: base_url, root: root} = tags
    test_plan_id = mod.test_plan_info()["id"]

    assert %{"id" => module_id} =
             CC.create_test_from_plan(client, test_plan_id, test_name)

    try do
      CC.wait_for_status(client, module_id, ["WAITING"])
      CC.start_test(client, module_id)

      module_status = CC.module_status(client, module_id)

      log =
        run_task(
          task,
          module_status,
          mod.config(),
          mod.ueberauth_provider_configuration_opts()
        )

      info = CC.wait_for_status(client, module_id, ["FINISHED"])
      assert %{"status" => "FINISHED", "result" => "PASSED"} = info
      File.write!(Path.join([root, "client-data", "#{test_name}.log"]), log)
    rescue
      e ->
        IO.puts("Log results URL: #{base_url}/log-detail.html?log=#{module_id}")

        reraise e, __STACKTRACE__
    end
  end

  def update_client_redirect_uri(client_config, provider) do
    redirect_uri =
      URI.to_string(
        URI.merge(
          UeberauthOidccCertificationWeb.Endpoint.url(),
          "/auth/#{provider}/callback"
        )
      )

    Map.put(client_config, "redirect_uri", redirect_uri)
  end

  def run_task(:login, status, config, opts) do
    start_provider_configuration(status, config, opts)
    session = new_session()
    visit(session, "/auth/#{config["alias"]}")

    assert_receive {:success, auth}
    inspect(auth)
  end

  def run_task(:login_with_account, status, config, opts) do
    provider = start_provider_configuration(status, config, opts)
    session = new_session()
    visit(session, "/auth/#{config["alias"]}")

    assert_receive {:success, auth}

    url = status["exposed"]["accounts_endpoint"]

    {_, client_configuration} = Application.get_env(:ueberauth, Ueberauth)[:providers][provider]

    {:ok, client_context} =
      Oidcc.ClientContext.from_configuration_worker(
        provider,
        client_configuration[:client_id],
        client_configuration[:client_secret],
        Map.new(client_configuration)
      )

    # support versions of Oidcc which did not have `type` as an Access Token
    # struct param
    token =
      Map.put(
        %Oidcc.Token.Access{token: auth.credentials.token},
        :type,
        auth.credentials.token_type
      )

    headers =
      Oidcc.Token.Access.authorization_headers(
        token,
        :get,
        url,
        client_context
      )

    connect_opts =
      case client_configuration[:request_opts] do
        %{ssl: ssl_opts} ->
          [transport_opts: ssl_opts]

        _ ->
          []
      end

    resp =
      case Req.get(
             url: url,
             headers: headers,
             connect_options: connect_opts
           ) do
        {:ok, %{status: 200}} = resp ->
          resp

        {:ok, %{headers: %{"dpop-nonce" => [dpop_nonce]}}} ->
          headers =
            Oidcc.Token.Access.authorization_headers(
              token,
              :get,
              url,
              client_context,
              %{dpop_nonce: dpop_nonce}
            )

          Req.get(url: url, headers: headers, connect_options: connect_opts)

        resp ->
          resp
      end

    assert {:ok, %{status: 200, body: body}} = resp

    inspect(auth) <> "\n" <> inspect(body)
  end

  def run_task(:login_twice, status, config, opts) do
    start_provider_configuration(status, config, opts)
    session = new_session()
    session = visit(session, "/auth/#{config["alias"]}")

    assert_receive {:success, auth1}

    visit(session, "/auth/#{config["alias"]}")

    assert_receive {:success, auth2}

    inspect(auth1) <> "\n" <> inspect(auth2)
  end

  def run_task(:login_fail, status, config, opts) do
    start_provider_configuration(status, config, opts)
    session = new_session()
    visit(session, "/auth/#{config["alias"]}")

    assert_receive {:failure, fail}
    inspect(fail)
  end

  def run_task(:fetch_configuration, status, _config, opts) do
    assert {:ok, {provider_configuration, _}} =
             Oidcc.ProviderConfiguration.load_configuration(status["exposed"]["issuer"], opts)

    inspect(provider_configuration)
  end

  def run_task(:fetch_jwks, status, _config, opts) do
    {:ok, {provider_config, _expiry}} =
      Oidcc.ProviderConfiguration.load_configuration(status["exposed"]["issuer"], opts)

    {:ok, jwks} = Oidcc.ProviderConfiguration.load_jwks(provider_config.jwks_uri, opts)
    inspect(jwks)
  end

  def run_task(:fail_configuration, status, _config, opts) do
    assert {:error, e} =
             Oidcc.ProviderConfiguration.load_configuration(status["exposed"]["issuer"], opts)

    inspect({:error, e})
  end

  def start_provider_configuration(status, config, opts) do
    PubSub.subscribe(@pubsub, "provider:#{config["alias"]}")
    provider = :"#{config["alias"]}"

    start_supervised!(
      Supervisor.child_spec(
        {
          Oidcc.ProviderConfiguration.Worker,
          %{
            name: provider,
            issuer: status["exposed"]["issuer"],
            provider_configuration_opts: opts
          }
        },
        id: provider
      )
    )

    provider
  end

  def new_session do
    {:ok, session} = Wallaby.start_session()

    on_exit(fn ->
      try do
        Wallaby.end_session(session)
      rescue
        MatchError -> :ok
      end
    end)

    session
  end

  def inspect(value) do
    Kernel.inspect(value, pretty: true, limit: :infinity, width: 100)
  end
end
