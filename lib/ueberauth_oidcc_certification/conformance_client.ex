defmodule UeberauthOidccCertification.ConformanceClient do
  @moduledoc """
  Wrapper around OpenidConformanceApi to provide a simpler interface.

  Based on the script in https://gitlab.com/openid/conformance-suite/-/blob/master/scripts/conformance.py
  """

  alias OpenidConformanceApi.Api.{LogApi, TestInfoApi, TestPlanApi, TestRunner}

  def with_config_lock(fun) do
    case Agent.start_link(fn -> :ok end, name: __MODULE__) do
      {:ok, pid} ->
        Agent.get(pid, fn _ -> fun.() end)
        Agent.stop(pid)

      {:error, {:already_started, _}} ->
        Process.sleep(Enum.random(0..100))
        with_config_lock(fun)
    end
  end

  def new do
    OpenidConformanceApi.Connection.new()
  end

  def create_test_plan(client, name, configuration, variant \\ nil) do
    opts =
      if variant do
        [variant: Jason.encode!(variant)]
      else
        []
      end

    case TestPlanApi.create_test_plan(
           client,
           name,
           configuration,
           opts
         ) do
      {:ok, plan} ->
        plan

      {:error, %{status: 500, body: body}} ->
        %{"trace" => trace} = Jason.decode!(body)
        raise RuntimeError, trace
    end
  end

  def test_plans_for_user(client) do
    {:ok, plans} =
      TestPlanApi.get_test_plans_for_current_user(client,
        draw: 2,
        start: 0,
        length: 500,
        order: "started,desc"
      )

    plans["data"]
  end

  def test_plan_info(client, name) do
    {:ok, plan} = TestPlanApi.get_test_plan_info(client, name)
    plan
  end

  def test_plan(client, plan_id) do
    {:ok, plan} = TestPlanApi.get_test_plan(client, plan_id)
    plan
  end

  # def create_test(client, test_name, configuration) do

  def create_test_from_plan(client, plan_id, test_name, variant \\ nil) do
    opts = [
      plan: plan_id
    ]

    opts =
      if variant do
        Keyword.put(opts, :variant, Jason.encode!(variant))
      else
        opts
      end

    {:ok, test} = TestRunner.create_test(client, test_name, opts)
    test
  end

  def module_status(client, module_id) do
    {:ok, status} = TestRunner.get_test_status(client, module_id)
    status
  end

  def module_info(client, module_id) do
    {:ok, info} = TestInfoApi.get_test_info(client, module_id)
    info
  end

  def test_log(client, plan_id) do
    {:ok, log} = LogApi.export_logs_of_plan(client, plan_id)
    log
  end

  def start_test(client, module_id) do
    {:ok, test} = TestRunner.start_test(client, module_id)
    test
  end

  def wait_for_status(client, module_id, expected, timeout \\ 30) when is_binary(module_id) do
    start = System.monotonic_time(:second)
    info = module_info(client, module_id)

    cond do
      info["status"] in expected ->
        info

      info["status"] in ["FAILED", "INTERRUPTED"] ->
        info

      true ->
        Process.sleep(1000)
        elapsed = System.monotonic_time(:second) - start

        if elapsed >= timeout do
          raise "Timeout"
        else
          wait_for_status(client, module_id, expected, timeout - elapsed)
        end
    end
  end

  def find_or_create_test_plan(client, test_plan, variant, config) do
    plans = test_plans_for_user(client)

    matching =
      Enum.find(plans, fn plan ->
        is_nil(plan["immutable"]) and
          plan["planName"] == test_plan and
          plan["variant"] == variant and
          plan["description"] == config["description"] and
          plan["config"] == config
      end)

    if matching do
      Map.put(matching, "id", matching["_id"])
    else
      %{"id" => _} = create_test_plan(client, test_plan, config, variant)
    end
  end
end
