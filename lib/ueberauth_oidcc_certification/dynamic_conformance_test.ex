defmodule UeberauthOidccCertification.DynamicConformanceTest do
  alias UeberauthOidccCertification.ConformanceClient, as: CC
  use Wallaby.DSL
  import ExUnit.Assertions
  import ExUnit.Callbacks
  alias Oidcc.ProviderConfiguration.Worker
  alias Phoenix.PubSub

  @pubsub UeberauthOidccCertification.PubSub

  defmacro __using__(opts) do
    quote bind_quoted: [opts: opts] do
      defmodule String.to_atom(opts[:describe] || opts[:test_plan]) do
        @moduledoc false
        use ExUnit.Case, async: true
        if opts[:moduletag], do: @moduletag(opts[:moduletag])

        import UeberauthOidccCertification.DynamicConformanceTest
        require Logger

        alias UeberauthOidccCertification.ConformanceClient, as: CC

        @test_plan opts[:test_plan]
        @ueberauth_provider_configuration_opts opts[:ueberauth_provider_configuration_opts] || %{}
        @ueberauth_config opts[:ueberauth_config] || []
        @config opts[:config]
                |> Map.merge(%{
                  "waitTimeoutSeconds" =>
                    Application.compile_env(:ueberauth_oidcc_certification, :module_timeout)
                })
                |> Map.update!("alias", fn a ->
                  "#{a}#{UeberauthOidccCertification.unique_suffix()}"
                end)
                |> Map.update!("description", fn d ->
                  "#{d} (#{UeberauthOidccCertification.version()})"
                end)
        @variant opts[:variant]
        @default_task opts[:default_task] || :login
        @overrides opts[:overrides] || %{}

        setup_all do
          client = CC.new()
          {:ok, client: client}
        end

        test_plan_info =
          if opts[:moduletag] == :skip do
            %{"modules" => []}
          else
            CC.find_or_create_test_plan(
              CC.new(),
              @test_plan,
              @variant,
              @config
            )
          end

        describe "(#{test_plan_info["id"]})" do
          for module <- test_plan_info["modules"],
              test_name = module["testModule"],
              task = Map.get(@overrides, test_name, @default_task) do
            if task == :skip, do: @tag(:skip)
            @tag String.to_atom(test_name)
            test test_name, %{client: client} do
              test_plan_id = unquote(test_plan_info["id"])
              test_name = unquote(test_name)
              task = unquote(task)

              assert %{"id" => module_id} =
                       CC.create_test_from_plan(client, test_plan_id, test_name)

              # CC.start_test(client, module_id)
              CC.wait_for_status(client, module_id, ["WAITING"])

              module_status = CC.module_status(client, module_id)

              try do
                run_task(
                  task,
                  module_status,
                  @ueberauth_config,
                  @ueberauth_provider_configuration_opts
                )

                info = CC.wait_for_status(client, module_id, ["FINISHED"])

                if info["result"] in ["PASSED", "REVIEW"] do
                  :ok
                else
                  assert %{"status" => "FINISHED", "result" => "PASSED"} = info
                end
              rescue
                e ->
                  base_url =
                    Application.get_env(
                      :ueberauth_oidcc_certification,
                      :base_url,
                      "https://www.certification.openid.net"
                    )

                  IO.puts("Log results URL: #{base_url}/log-detail.html?log=#{module_id}")

                  reraise e, __STACKTRACE__
              end
            end
          end
        end
      end
    end
  end

  def run_task(:register, status, config, opts) do
    provider = start_provider_configuration(status, opts)
    register_client(provider, config)
  end

  def run_task(:login, status, config, opts) do
    provider = start_provider_configuration(status, opts)
    register_client(provider, config)
    session = new_session()
    visit(session, "/auth/#{provider}")
    assert_receive {:success, _auth}
  end

  def run_task(:login_logout, status, config, opts) do
    provider = start_provider_configuration(status, opts)
    register_client(provider, config)
    session = new_session()
    visit(session, "/auth/#{provider}")
    assert_receive {:success, auth}

    post_logout_redirect_uri =
      URI.to_string(
        URI.merge(
          UeberauthOidccCertificationWeb.Endpoint.url(),
          "/auth/#{provider}/logout"
        )
      )

    logout_state = Base.url_encode64(:crypto.strong_rand_bytes(16), padding: false)

    {:ok, logout_url} =
      UeberauthOidcc.initiate_logout_url(
        auth,
        %{
          post_logout_redirect_uri: post_logout_redirect_uri,
          state: logout_state
        }
      )

    visit(session, logout_url)
    assert_receive {:logout, %{"frontchannel" => _}}
    # per
    # https://gitlab.com/openid/conformance-suite/-/blob/master/src/main/resources/templates/oidccFrontChannelLogout.html#L46
    # the redirect takes 5s, plus a little wiggle room
    assert_receive {:logout, _}, 6_000
  end

  def run_task(:login_twice, status, config, opts) do
    provider = start_provider_configuration(status, opts)
    register_client(provider, config)
    session = new_session()
    session = visit(session, "/auth/#{provider}")
    assert_receive {:success, _auth}

    visit(session, "/auth/#{provider}")
    assert_receive {:success, _auth}
  end

  def run_task(:login_fail, status, config, opts) do
    provider = start_provider_configuration(status, opts)
    register_client(provider, config)
    session = new_session()
    visit(session, "/auth/#{provider}")
    assert_receive {:failure, _auth}
  end

  def run_task(:fetch_configuration, status, _config, opts) do
    assert {:ok, _} =
             Oidcc.ProviderConfiguration.load_configuration(status["exposed"]["issuer"], opts)
  end

  def run_task(:fetch_jwks, status, _config, opts) do
    {:ok, {provider_config, _expiry}} =
      Oidcc.ProviderConfiguration.load_configuration(status["exposed"]["issuer"], opts)

    {:ok, _} = Oidcc.ProviderConfiguration.load_jwks(provider_config.jwks_uri, opts)
  end

  def run_task(:fail_configuration, status, _config, opts) do
    assert {:error, _} =
             Oidcc.ProviderConfiguration.load_configuration(status["exposed"]["issuer"], opts)
  end

  def start_provider_configuration(status, opts) do
    provider = String.to_atom(status["id"])

    PubSub.subscribe(@pubsub, "provider:#{provider}")

    start_supervised!({
      Oidcc.ProviderConfiguration.Worker,
      %{
        name: provider,
        issuer: status["exposed"]["issuer"],
        provider_configuration_opts: opts
      }
    })

    provider
  end

  def register_client(provider, config) do
    redirect_uri =
      URI.to_string(
        URI.merge(
          UeberauthOidccCertificationWeb.Endpoint.url(),
          "/auth/#{provider}/callback"
        )
      )

    logout_uri =
      URI.to_string(
        URI.merge(
          UeberauthOidccCertificationWeb.Endpoint.url(),
          "/auth/#{provider}/logout"
        )
      )

    Application.ensure_all_started(:ueberauth_oidcc)

    provider_configuration =
      Worker.get_provider_configuration(provider)

    jwks =
      case Keyword.get(config, :client_jwks) do
        nil ->
          :undefined

        jwks ->
          JOSE.JWK.from_map(%{
            "keys" => [
              JOSE.JWK.to_public_map(jwks)
            ]
          })
      end

    token_auth_method = List.first(provider_configuration.token_endpoint_auth_methods_supported)

    token_signing_alg =
      case token_auth_method do
        "private_key_jwt" -> "ES256"
        "client_secret_jwt" -> "HS256"
        _ -> :undefined
      end

    {:ok, client_configuration} =
      Oidcc.ClientRegistration.register(
        provider_configuration,
        %Oidcc.ClientRegistration{
          application_type: :web,
          jwks: jwks,
          token_endpoint_auth_method: token_auth_method,
          token_endpoint_auth_signing_alg: token_signing_alg,
          redirect_uris: [redirect_uri],
          post_logout_redirect_uris: [logout_uri],
          extra_fields: %{
            "frontchannel_logout_uri" => logout_uri <> "?frontchannel"
          }
        }
      )

    CC.with_config_lock(fn ->
      ueberauth_config = Application.get_env(:ueberauth, Ueberauth)

      ueberauth_config =
        put_in(
          ueberauth_config[:providers][provider],
          {Ueberauth.Strategy.Oidcc,
           Keyword.merge(
             [
               issuer: provider,
               userinfo: true,
               scopes: ~w(openid profile),
               callback_methods: ["GET"],
               client_id: client_configuration.client_id,
               client_secret:
                 case client_configuration.client_secret do
                   :undefined -> Base.url_encode64(:crypto.strong_rand_bytes(32), padding: false)
                   secret -> secret
                 end
             ],
             config
           )}
        )

      Application.put_env(:ueberauth, Ueberauth, ueberauth_config)
    end)

    on_exit(provider, fn ->
      CC.with_config_lock(fn ->
        ueberauth_config = Application.get_env(:ueberauth, Ueberauth)

        ueberauth_config =
          update_in(ueberauth_config[:providers], &Keyword.delete(&1, provider))

        Application.put_env(:ueberauth, Ueberauth, ueberauth_config)
      end)
    end)

    provider
  end

  def new_session do
    {:ok, session} = Wallaby.start_session()

    on_exit(fn ->
      try do
        Wallaby.end_session(session)
      rescue
        MatchError -> :ok
      end
    end)

    session
  end
end
