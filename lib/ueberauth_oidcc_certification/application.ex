defmodule UeberauthOidccCertification.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      UeberauthOidccCertificationWeb.Telemetry,
      {Phoenix.PubSub, name: UeberauthOidccCertification.PubSub},
      # Start a worker by calling: UeberauthOidccCertification.Worker.start_link(arg)
      # {UeberauthOidccCertification.Worker, arg},
      # Start to serve requests, typically the last entry
      UeberauthOidccCertificationWeb.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: UeberauthOidccCertification.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    UeberauthOidccCertificationWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
