defmodule UeberauthOidccCertificationWeb.PageController do
  use UeberauthOidccCertificationWeb, :controller

  def home(conn, _params) do
    render(conn, :home,
      layout: false,
      page_title: "Home",
      current_user: get_session(conn, :current_user)
    )
  end

  def restart(conn, _params) do
    Application.stop(:ueberauth_oidcc)
    Application.ensure_all_started(:ueberauth_oidcc)

    conn
    |> put_flash(:info, "Restarted Ueberauth Oidcc.")
    |> redirect(to: "/")
  end

  def trigger(conn, _params) do
    Application.stop(:ueberauth_oidcc)
    Application.ensure_all_started(:ueberauth_oidcc)

    redirect(conn, to: "/auth/openid")
  end

  def logout(conn, %{"provider" => provider} = params) do
    Phoenix.PubSub.broadcast(
      UeberauthOidccCertification.PubSub,
      "provider:#{provider}",
      {:logout, params}
    )

    send_resp(conn, 200, Jason.encode_to_iodata!(%{ok: true}))
  end
end
