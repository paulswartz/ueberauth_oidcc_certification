defmodule UeberauthOidccCertificationWeb.AuthController do
  @moduledoc """
  Auth controller responsible for handling Ueberauth responses
  """

  use UeberauthOidccCertificationWeb, :controller

  require Logger
  import Phoenix.HTML, only: [raw: 1]

  def request(conn, %{"provider" => provider_bin}) do
    provider = String.to_existing_atom(provider_bin)

    {mod, config} =
      Application.get_env(:ueberauth, Ueberauth)[:providers][provider] ||
        {Ueberauth.Strategy.Oidcc, []}

    case Ueberauth.run_request(conn, provider, {mod, config}) do
      %Plug.Conn{assigns: %{ueberauth_failure: _}} = conn ->
        callback(conn, conn.params)

      conn ->
        conn
    end
  end

  def callback(%{assigns: %{ueberauth_failure: failures}} = conn, %{"provider" => provider}) do
    Logger.warning("failed to authenticate: reason=#{inspect(failures)}")

    Phoenix.PubSub.broadcast(
      UeberauthOidccCertification.PubSub,
      "provider:#{provider}",
      {:failure, failures}
    )

    errors =
      for error <- failures.errors do
        ["- ", error.message_key, ": ", inspect(error.message), raw("<br/>")]
      end

    conn
    |> put_flash(:error, ["Failed to authenticate:", raw("<br/>"), errors])
    |> redirect(to: "/")
  end

  def callback(%{assigns: %{ueberauth_auth: %{provider: :google} = auth}} = conn, _params) do
    claims = auth.extra.raw_info.user

    redirect_with_claims(conn, auth, claims)
  end

  def callback(
        %{assigns: %{ueberauth_auth: %{strategy: Ueberauth.Strategy.Oidcc} = auth}} = conn,
        _params
      ) do
    claims =
      Map.merge(
        auth.extra.raw_info.claims,
        auth.extra.raw_info.userinfo || %{}
      )

    redirect_with_claims(conn, auth, claims)
  end

  def callback(conn, %{"provider" => provider_bin}) do
    provider = String.to_existing_atom(provider_bin)
    {mod, config} = Application.get_env(:ueberauth, Ueberauth)[:providers][provider]
    conn = Ueberauth.run_callback(conn, provider, {mod, config})
    callback(conn, conn.params)
  end

  defp redirect_with_claims(conn, auth, claims) do
    Logger.info(inspect(auth, limit: :infinity))

    Phoenix.PubSub.broadcast(
      UeberauthOidccCertification.PubSub,
      "provider:#{auth.provider}",
      {:success, auth}
    )

    claims =
      for {key, value} <-
            Enum.sort(claims) do
        [inspect(key), ": ", inspect(value), raw("<br/>")]
      end

    conn
    |> put_flash(:info, ["Successfully authenticated:", raw("<br/>"), claims])
    |> redirect(to: "/")
  end
end
