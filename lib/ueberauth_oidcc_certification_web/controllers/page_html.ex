defmodule UeberauthOidccCertificationWeb.PageHTML do
  use UeberauthOidccCertificationWeb, :html

  embed_templates "page_html/*"
end
