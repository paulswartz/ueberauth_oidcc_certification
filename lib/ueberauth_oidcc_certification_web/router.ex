defmodule UeberauthOidccCertificationWeb.Router do
  use UeberauthOidccCertificationWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, html: {UeberauthOidccCertificationWeb.Layouts, :root}
    plug :put_secure_browser_headers
  end

  pipeline :csrf do
    plug :protect_from_forgery
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", UeberauthOidccCertificationWeb do
    pipe_through [:browser, :csrf]

    get "/", PageController, :home
    get "/restart", PageController, :restart
    get "/trigger", PageController, :trigger
    get "/auth/:provider/logout", PageController, :logout
  end

  scope "/auth", UeberauthOidccCertificationWeb do
    pipe_through [:browser, :csrf]

    get "/:provider", AuthController, :request
    get "/:provider/callback", AuthController, :callback
  end

  scope "/auth", UeberauthOidccCertificationWeb do
    pipe_through [:browser]

    post "/:provider/callback", AuthController, :callback
  end

  # Enable LiveDashboard in development
  if Application.compile_env(:ueberauth_oidcc_certification, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: UeberauthOidccCertificationWeb.Telemetry
    end
  end
end
