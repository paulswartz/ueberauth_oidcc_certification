defmodule UeberauthOidccCertificationWeb.Layouts do
  use UeberauthOidccCertificationWeb, :html

  embed_templates "layouts/*"
end
