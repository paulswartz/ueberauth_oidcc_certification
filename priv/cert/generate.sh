#!/usr/bin/env sh
set -e

openssl ecparam -genkey -name P-256 -noout -out selfsigned_key.pem
openssl req -new -sha256 -key selfsigned_key.pem -out selfsigned.csr   -subj "/CN=UeberauthOidccCertification"
openssl req -x509 -sha256 -days 3650 -key selfsigned_key.pem -in selfsigned.csr -out selfsigned.pem
openssl req -in selfsigned.csr -text -noout
