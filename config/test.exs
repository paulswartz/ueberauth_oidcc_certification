import Config

port = 4002

config :ueberauth_oidcc_certification, UeberauthOidccCertificationWeb.Endpoint,
  https: [
    otp_app: :ueberauth_oidcc_certification,
    ip: {127, 0, 0, 1},
    port: port,
    cipher_suite: :strong,
    keyfile: "priv/cert/selfsigned_key.pem",
    certfile: "priv/cert/selfsigned.pem"
  ],
  secret_key_base: "KAINXtQ6jDhHS6Flnjp22maHAl/+p9Cu2YVuupEbSyXH5hghzYTMa6AR0r4/N+01",
  server: true

config :ueberauth_oidcc,
  issuers: [],
  providers: []

# Print only errors during test
config :logger, level: :error

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime

config :wallaby,
  base_url: "https://localhost:#{port}",
  otp_app: :ueberauth_oidcc_certification,
  screenshot_on_failure: false,
  chromedriver: [
    capabilities: %{
      chromeOptions: %{
        args: [
          "--no-sandbox",
          "window-size=1280,800",
          "--disable-gpu",
          "--headless",
          "--fullscreen",
          "--user-agent=Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36",
          # allow HTTPS with a self-signed certificate
          "--allow-insecure-localhost"
        ]
      }
    }
  ]
