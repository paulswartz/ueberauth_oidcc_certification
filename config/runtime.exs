import Config

# config/runtime.exs is executed for all environments, including
# during releases. It is executed after compilation and before the
# system starts, so it is typically used to load production configuration
# and secrets from environment variables or elsewhere. Do not define
# any compile-time configuration in here, as it won't be applied.
# The block below contains prod specific runtime configuration.

# ## Using releases
#
# If you use `mix release`, you need to explicitly enable the server
# by passing the PHX_SERVER=true when you start it:
#
#     PHX_SERVER=true bin/ueberauth_oidcc_certification start
#
# Alternatively, you can use `mix phx.gen.release` to generate a `bin/server`
# script that automatically sets the env var above.
if System.get_env("PHX_SERVER") do
  config :ueberauth_oidcc_certification, UeberauthOidccCertificationWeb.Endpoint, server: true
end

config :ueberauth_oidcc_certification,
  token: System.get_env("CONFORMANCE_TOKEN")

if root = System.get_env("CONFORMANCE_ROOT") do
  config :ueberauth_oidcc_certification,
    base_url: root
end

if issuer = System.get_env("ISSUER") do
  config :ueberauth_oidcc,
    issuers: [
      %{
        name: :openid_issuer,
        issuer: issuer
      }
    ],
    providers: [
      openid: [
        client_id: System.fetch_env!("CLIENT_ID"),
        client_secret: System.fetch_env!("CLIENT_SECRET")
      ]
    ]
end

if config_env() == :test do
  config :ueberauth_oidcc, issuers: []
end

if selenium_host = System.get_env("SELENIUM_HOST") do
  config :wallaby,
    driver: Wallaby.Selenium,
    selenium: [
      remote_url: "http://#{selenium_host}:4444/wd/hub/"
    ]
end

if System.get_env("CI") do
  config :ex_unit, assert_receive_timeout: 500

  config :ueberauth_oidcc_certification,
    module_timeout: 10
end
